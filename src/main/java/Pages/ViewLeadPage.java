package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	public ViewLeadPage()
	{
     PageFactory.initElements(driver, this);
	}	
	
	@FindBy(linkText ="CRM/SFA")
	WebElement eleCrmsfa;
	public MyHomePage clickCrmsfa()
	{
		click(eleCrmsfa);
		return new MyHomePage();
	}

}