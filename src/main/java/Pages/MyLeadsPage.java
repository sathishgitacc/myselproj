package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {
	public MyLeadsPage()
	{
     PageFactory.initElements(driver, this);
	}	
	
	@FindBy(linkText ="Create Lead")
	WebElement eleCrateLead;
	public MyLeadsPage clickCreatelead()
	{
		click(eleCrateLead);
		return this;
	}
	
	@FindBy(id = "createLeadForm_companyName")
	WebElement eleCN;
	public MyLeadsPage enterCompanyName(String data)
	{
		return this;
	}
	
	@FindBy(id = "createLeadForm_firstName")
	WebElement eleFN;
	public MyLeadsPage enterFirstName(String data)
	{
		type(eleFN,data);
		return this;
	}
	@FindBy(id = "createLeadForm_firstName")
	WebElement eleLN;
	public MyLeadsPage enterLastName(String data)
	{
		type(eleLN,data);
		return this;
	}
	
	@FindBy(className = "smallSubmit")
	WebElement eleCreateLeadButton;
	public MyLeadsPage clickSubmit()
	{
		click(eleCreateLeadButton);
		return this;
	}

}