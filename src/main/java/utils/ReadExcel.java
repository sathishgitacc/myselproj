package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String fileName)throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook("./data/CL.xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row Count " + "" + rowCount);
		int columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column Count " + "" + columnCount);
		Object[][] data = new Object[rowCount][columnCount];
		for (int i=1;i<=rowCount;i++)
		{
			XSSFRow row =sheet.getRow(i);
			{
				for (int j=0;j<columnCount;j++)
				{
					XSSFCell cell =row.getCell(j);
					data[i-1][j] = cell.getStringCellValue();
					//String cellValue = cell.getStringCellValue();
					//System.out.println(cellValue);
				}
			}
			
			
		}
		return data;
		
		

	}

}
