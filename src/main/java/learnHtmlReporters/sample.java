package learnHtmlReporters;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class sample {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports ex = new ExtentReports();
		ex.attachReporter(html);
		ExtentTest test = ex.createTest("CreateLead", "CreateLeafRaps");
		test.assignCategory("reg");
		test.assignAuthor("me");
		test.pass("Browser Launched Succesfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		ex.flush();
		

	}

}
