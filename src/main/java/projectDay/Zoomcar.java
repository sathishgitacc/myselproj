package projectDay;

import java.awt.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Zoomcar {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Start your wonderful journey")).click();
		driver.findElement(By.xpath("//div[@class='component-popular-locations']/div[5]")).click();
		driver.findElement(By.xpath("//button[@class='proceed']")).click();
		// Get the current date
				Date date = new Date();
		// Get only the date (and not month, year, time etc)
				DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
				String today = sdf.format(date);
		// Convert to integer and add 1 to it
				int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
				System.out.println(tomorrow);
				
		
		driver.findElement(By.xpath("//div[@class='days']/div[2]")).click();
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		driver.findElement(By.xpath("//button[text()='Done']")).click();
	//int size = driver.findElements(By.xpath("//div[@class='car-item-policies']")).size();
		int size = driver.findElements(By.xpath("//div[@class='img']")).size();
	    System.out.println(size);
	    java.util.List<WebElement> pr = driver.findElements(By.xpath("//div[@class='price']"));
	    ArrayList<String> lList = new ArrayList<String>();
	    for (int i = 0; i<pr.size(); i=i+1) 
        {
        String s = pr.get(i).getText();    
        String s1= s.replaceAll("[^a-zA-Z0-9]","");
        lList.add(s1);
        } 
		Collections.sort(lList);
	    System.out.println(lList);
	    System.out.println(Collections.max(lList));
	    String maxvalue = Collections.max(lList);
	    String carname = driver.findElement(By.xpath("//div[contains(text(),"+maxvalue+")]/../../../div[2]/h3")).getText();
	    System.out.println(carname);
	    driver.findElement(By.xpath("//div[contains(text(),"+maxvalue+")]/following-sibling::button")).click();
	    //driver.close();

	}

}
